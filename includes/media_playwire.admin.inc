<?php
/**
 * @file
 * Media: Playwire module settings UI.
 */

/**
 * Build the Playwire configuration form.
 */
function media_playwire_admin_settings() {
  $form = array();
  // Playwire API token
  $form['media_playwire_api_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Playwire API Token'),
    '#default_value' => variable_get('media_playwire_api_token', ''),
  );
  // Embedding method
  $form['media_playwire_embed_method'] = array(
    '#type' => 'select',
    '#title' => t('Choose an embed method'),
    '#options' => array(
      0 => t('Iframe'),
      1 => t('JavaScript'),
      2 => t('HTML Embed'),
    ),
    '#default_value' => variable_get('media_playwire_embed_method', 2),
  );
  return system_settings_form($form);
}
