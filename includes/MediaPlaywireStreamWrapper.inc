<?php

/**
 *  @file
 *  Create a Playwire Stream Wrapper class for the Media/Resource module.
 */

/**
 *  Create an instance like this:
 *  $playwire = new MediaPlaywireStreamWrapper('playwire://video/[video-code]');
 */
class MediaPlaywireStreamWrapper extends MediaReadOnlyStreamWrapper {
  protected $base_url = 'http://playwire.com/';
  
  /**
   * Return the mimetype for Playwire videos.
   * 
   * @param type $uri
   * @param type $mapping
   * @return type 'video/playwire'
   */
  static function getMimeType($uri, $mapping = NULL) {
    return 'video/playwire';
  }
  
  function interpolateUrl() {
    if ($parameters = $this->get_parameters()) {
      return $this->base_url . 'videos/' . $parameters['videos'];
    }
  }
  
  /**
   * Return a URL pointing to the thumbnail hosted by Playwire.
   */
  function getOriginalThumbnailPath() {
    $provider = media_internet_get_provider($this->interpolateUrl());
    $video = $provider->getVideoInfo();
    return $video->thumb_url;
  }

  /**
   * Return a the path to a local copy of the thumbnail image. Locally hosted
   * images can be run through Image Styles and such.
   */
  function getLocalThumbnailPath() {
    $parts = $this->get_parameters();
    $local_path = 'public://media-playwire/' . check_plain($parts['videos']) . '.png';
    if (!file_exists($local_path)) {
      $dirname = drupal_dirname($local_path);
      file_prepare_directory($dirname, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      @copy($this->getOriginalThumbnailPath(), $local_path);
    }
    return $local_path;
  }
}
