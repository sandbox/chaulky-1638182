<?php

/**
 * @file media_playwire/includes/themes/media_playwire.theme.inc
 *
 * Theme and preprocess functions for Media: Playwire.
 */

/**
 * Preprocess function for theme('media_playwire_video').
 */
function media_playwire_preprocess_media_playwire_video(&$variables) {
  // Get the Playwire video id.
  $uri = $variables['uri'];
  $wrapper = file_stream_wrapper_get_instance_by_uri($uri);
  $parts = $wrapper->get_parameters();
  $variables['video_id'] = check_plain($parts['videos']);
  
  // @TODO The embed code is the same for all videos, except for the video id.
  // I should cache the embed code the first time I grab it, then get it from
  // cache here instead of calling the service again
  $provider = media_internet_get_provider($wrapper->interpolateUrl());
  $video = $provider->getVideoInfo();
  
  switch (variable_get('media_playwire_embed_method', 2)) {
    case 0:
      $embed_code = $video->iframe_embed_code;
      break;
    case 1:
      // @TODO How can I control the sizing when using the JavaScript embed?
      //$embed_code = $video->iframe_embed_code;
      //break;
    case 2:
      $embed_code = $video->embed_code;
      break;
  }
  
  // @TODO I only need to replace the width/height if they aren't using the
  // defaults set on Playwire. Though, videos on Playwire can each have a
  // non-default size set. So we may not have consistency of video size on the
  // site if we use whatever values are pulled from the server. Also, I'd have
  // to cache embed codes for each video, or sizes for each video, to know
  // what size was set on Playwire.
  $patterns = array(
    '~(width=")(\d+)~', // width
    '~(height=")(\d+)~', // height
    //'~(playwire_player_)(\d+)~', // video id
    //'~(/embed/)(\d+)~', // video id in url
  );
  $replacements = array(
    '${1}' . $variables['width'], // width replacement
    '${1}' . $variables['height'], // height replacement
    //'${1}' . $variables['video_id'], // video id replacement
    //'${1}' . $variables['video_id'], // video id in url replacement
   );
  $variables['embed'] = preg_replace($patterns, $replacements, $embed_code);
}
