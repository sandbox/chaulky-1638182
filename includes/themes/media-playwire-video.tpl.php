<?php

/**
 * @file media_playwire/includes/themes/media-playwire-video.tpl.php
 *
 * Template file for theme('media_playwire_video').
 *
 * Variables available:
 *  $uri - The uri to the Playwire video, such as playwire://videos/70244.
 *  $embed - The embed code to play the video using Playwire's player.
 *
 * Note that we set the width & height of the outer wrapper manually so that
 * the JS will respect that when resizing later.
 */
?>
<div class="media-playwire-outer-wrapper" id="media-playwire-<?php print $video_id; ?>">
  <div class="media-playwire-preview-wrapper">
    <?php print $embed; ?>
  </div>
</div>
