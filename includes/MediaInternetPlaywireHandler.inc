<?php

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers().
 */
class MediaInternetPlaywireHandler extends MediaInternetBaseHandler {
  /**
   * Parse the given embed code to be processed by this handler.
   * 
   * @param type $embedCode
   * @return type 
   */
  public function parse($embedCode) {
    $patterns = array(
      '@playwire\.com/videos/([^"\&\? ]+)@i',
    );
    foreach ($patterns as $pattern) {
      preg_match($pattern, $embedCode, $matches);
      if (isset($matches[1])) { // removed call to valid_id for now
        return file_stream_wrapper_uri_normalize('playwire://videos/' . $matches[1]);
      }
    }
  }
  
  public function getFileObject() {
    $uri = $this->parse($this->embedCode);
    $file = file_uri_to_object($uri, TRUE);

    if (empty($file->fid) && $info = $this->getVideoInfo()) {
      $file->filename = truncate_utf8($info->name, 255);
    }

    return $file;
  }
  
  /**
   * Recognize if this handler should take the the item with the embed
   * coded passed as argument.
   *
   * @param string $embed_code
   *
   * @return boolean
   *   Whether or not this handler is resposible for the give embed code.
   */
  public function claim($embedCode) {
    if ($this->parse($embedCode)) {
      return TRUE;
    }
  }
  
  /**
   * Get information about the video from the Playwire webservice.
   */
  public function getVideoInfo() {
    $uri = $this->parse($this->embedCode);
    $video_id = arg(1, file_uri_target($uri));
    $api_token = variable_get('media_playwire_api_token', NULL);
    $playwire = new Playwire($api_token);
    $video = $playwire->getVideo($video_id);
    
    return $video;
  }
}
