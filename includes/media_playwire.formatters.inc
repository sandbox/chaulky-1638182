<?php

/**
 * @file
 * Provide display formatters for Playwire videos.
 */

/**
 * Implements hook_file_formatter_info().
 */
function media_playwire_file_formatter_info() {
  $formatters['media_playwire_video'] = array(
    'label' => t('Playwire Video'),
    'file types' => array('video'),
    // @TODO pull defaults from the Playwire service, or leave blank to use
    // values returned from Playwire service
    'default settings' => array(
      'width' => 560,
      'height' => 315,
    ),
    'view callback' => 'media_playwire_file_formatter_video_view',
    'settings callback' => 'media_playwire_file_formatter_video_settings',
  );
  
  $formatters['media_playwire_image'] = array(
    'label' => t('Playwire Preview Image'),
    'file types' => array('video'),
    'default settings' => array(
      'image_style' => '',
    ),
    'view callback' => 'media_playwire_file_formatter_image_view',
    'settings callback' => 'media_playwire_file_formatter_image_settings',
  );
  
  return $formatters;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_playwire_file_formatter_video_view($file, $display, $langcode) {
  $scheme = file_uri_scheme($file->uri);
  // WYSIWYG does not yet support video inside a running editor instance.
  if ($scheme == 'playwire' && empty($file->override['wysiwyg'])) {
    $element = array(
      '#theme' => 'media_playwire_video',
      '#uri' => $file->uri,
      '#width' => $display['settings']['width'],
      '#height' => $display['settings']['height'],
    );
    
    foreach (array('width', 'height') as $setting) {
      $element['#options'][$setting] = isset($file->override[$setting]) ? $file->override[$setting] : $display['settings'][$setting];
    }
    return $element;
  }
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function media_playwire_file_formatter_video_settings($form, &$form_state, $settings) {
  $element = array();
  $element['width'] = array(
    '#title' => t('Width'),
    '#type' => 'textfield',
    '#size' => 10, // overridden by CSS for vertical tabs
    '#default_value' => $settings['width'],
  );
  $element['height'] = array(
    '#title' => t('Height'),
    '#type' => 'textfield',
    '#size' => 10, // overridden by CSS for vertical tabs
    '#default_value' => $settings['height'],
  );
  return $element;
}

/**
 * Implements hook_file_formatter_FORMATTER_view().
 */
function media_playwire_file_formatter_image_view($file, $display, $langcode) {
  $scheme = file_uri_scheme($file->uri);
  if ($scheme == 'playwire') {
    $wrapper = file_stream_wrapper_get_instance_by_uri($file->uri);
    $image_style = $display['settings']['image_style'];
    $valid_image_styles = image_style_options(FALSE);
    if (empty($image_style) || !isset($valid_image_styles[$image_style])) {
      $element = array(
        '#theme' => 'image',
        '#path' => $wrapper->getOriginalThumbnailPath(),
      );
    }
    else {
      $element = array(
        '#theme' => 'image_style',
        '#style_name' => $image_style,
        '#path' => $wrapper->getLocalThumbnailPath(),
      );
    }
    return $element;
  }
}

/**
 * Implements hook_file_formatter_FORMATTER_settings().
 */
function media_playwire_file_formatter_image_settings($form, &$form_state, $settings) {
  $element = array();
  $element['image_style'] = array(
    '#title' => t('Image style'),
    '#type' => 'select',
    '#options' => image_style_options(FALSE),
    '#default_value' => $settings['image_style'],
    '#empty_option' => t('None (original image)'),
  );
  return $element;
}
